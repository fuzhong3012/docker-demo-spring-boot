FROM java:8
VOLUME /tmp
ADD app-1.0.0.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java","-jar","/app.jar"]

