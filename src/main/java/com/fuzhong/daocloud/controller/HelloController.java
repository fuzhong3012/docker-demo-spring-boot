package com.fuzhong.daocloud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String firstDemo(){
        return "Hello! This is my first Docker program !";
    }

    @RequestMapping("/hello")
    public String hello(){
        return "Hello! Docker!!!!!!!";
    }

}
